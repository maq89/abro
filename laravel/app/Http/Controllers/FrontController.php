<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{

    public function __construct()
    {
    }


    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = array(
                'vat_number' => $request->input('vat_number')
            );
            $payload = json_encode($data);
            $ch = curl_init('https://test23.saasc.uk/api/vat-validation');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer PFxf5eUGicWgZJkkWjDqeoH8soAUGV8g3J8H4oKCixaQJtUscMujPOp9ouWh'
            ));
            $result = curl_exec($ch);
            curl_close($ch);

            return view('search', ['result' => json_decode($result)]);
        }

        return view('search');

    }
}