<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class IndexController extends Controller
{
    public function vatValidation(Request $request){

        $validator = Validator::make($request->all(), [
            'vat_number'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $number = $request->input('vat_number');
        $vat_number = \App\VatNumber::where([
            'number' => $number
        ])->first();

        if ($vat_number) {
            return response()->json([
                'vat_number' =>  $vat_number->number,
                'status'     => ($vat_number->status == 1) ? 'Active' : 'Expire'
            ], 200);
        }

        return response()->json([
            'vat_number' => $number,
            'status'     => 'Invalid'
        ], 200);

    }

    public function test(){
        return 'test';
    }
}