<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Search Vat</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<body>

<form action="" method="post">

    <div class="col-lg-6 col-lg-offset-3" style="margin-top: 30px">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Validate VAT</div>
            </div>
            <div class="panel-body">
                <div class="verify-box">
                    <div class="form-group">
                        <input type="text" class="form-control" name="vat_number"  placeholder="Enter VAT Number" required="required">
                    </div>
                    <div class="form-group ">
                        <div class="cols-sm-4">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="cols-sm-8 pull-left">
                            @if(isset($result))
                                <div class="alert-warning">
                                    <strong>Vat Number : </strong>{{ $result->vat_number }} <br>
                                    <strong>Status : </strong>{{ $result->status }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

</body>
</html>


